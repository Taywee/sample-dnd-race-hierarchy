/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */

#include "human.hxx"
#include <random>

Human::Human() {
}

std::string_view Human::race_name() {
    return "Human";
}

std::unique_ptr<Race> Human::generate() {
    return std::make_unique<Human>();
}

std::string_view Human::generate_character_name() {
    static std::default_random_engine engine{std::random_device{}()};
    static std::uniform_int_distribution<std::uint8_t> distribution{0, 1};

    switch (distribution(engine)) {
        case 0:
            return "Adam";

        case 1:
            return "Eve";
    }
    __builtin_unreachable();
}
