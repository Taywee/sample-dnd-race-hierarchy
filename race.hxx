/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */
#pragma once

#include <string_view>
#include <memory>

class Race {
    public:
        virtual std::string_view race_name() = 0;
        virtual ~Race() = 0;

        virtual std::string_view generate_character_name() = 0;

        static std::unique_ptr<Race> generate();
};
