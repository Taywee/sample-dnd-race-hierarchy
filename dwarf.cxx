/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */

#include "dwarf.hxx"
#include <random>

#include "hill_dwarf.hxx"

Dwarf::Dwarf() {
}

std::string_view Dwarf::race_name() {
    return "Dwarf";
}

std::unique_ptr<Race> Dwarf::generate() {
    static std::default_random_engine engine{std::random_device{}()};
    static std::uniform_int_distribution<std::uint8_t> distribution{0, 1};

    switch (distribution(engine)) {
        case 0:
            return std::make_unique<Dwarf>();
        case 1:
            return HillDwarf::generate();
    }
    __builtin_unreachable();
}

std::string_view Dwarf::generate_character_name() {
    static std::default_random_engine engine{std::random_device{}()};
    static std::uniform_int_distribution<std::uint8_t> distribution{0, 1};

    switch (distribution(engine)) {
        case 0:
            return "Urist";

        case 1:
            return "Gralmir";
    }
    __builtin_unreachable();
}
