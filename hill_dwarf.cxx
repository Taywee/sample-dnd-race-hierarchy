/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */

#include "hill_dwarf.hxx"
#include <random>

HillDwarf::HillDwarf() {
}

std::string_view HillDwarf::race_name() {
    return "Hill Dwarf";
}

std::unique_ptr<Race> HillDwarf::generate() {
    return std::make_unique<HillDwarf>();
}

std::string_view HillDwarf::generate_character_name() {
    static std::default_random_engine engine{std::random_device{}()};
    static std::uniform_int_distribution<std::uint8_t> distribution{0, 1};

    switch (distribution(engine)) {
        case 0:
            return "HillUrist";

        case 1:
            return "HillGralmir";
    }
    __builtin_unreachable();
}
