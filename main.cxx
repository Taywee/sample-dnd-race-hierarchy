/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */

#include <iostream>

#include "character.hxx"

int main(int argc, char **argv)
{
    Character character;
    std::cout << character.get_name() << ": " << character.get_race().race_name() << std::endl;
    return 0;
}

