/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */
#pragma once

#include "race.hxx"
#include <string_view>

class Dwarf : public Race {
    public:
        Dwarf();

        std::string_view race_name() override;

        static std::unique_ptr<Race> generate();

        std::string_view generate_character_name() override;
};
