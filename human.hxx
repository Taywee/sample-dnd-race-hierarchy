/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */
#pragma once

#include "race.hxx"
#include <string_view>

class Human : public Race {
    public:
        Human();

        std::string_view race_name() override;

        std::string_view generate_character_name() override;

        static std::unique_ptr<Race> generate();
};
