/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */
#pragma once

#include <string>
#include <string_view>
#include <memory>
#include "race.hxx"

class Character {
    private:
        std::unique_ptr<Race> race;
        std::string name;

    public:
        /** Get a character with a random race and random name.
         */
        Character();

        Character(std::unique_ptr<Race> race, std::string name);

        std::string_view get_name();

        const Race &get_race() const;
        Race &get_race();
};
