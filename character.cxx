/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */

#include "character.hxx"

Character::Character() : race(Race::generate()), name(race->generate_character_name()) {
}

Character::Character(std::unique_ptr<Race> race, std::string name) : race(std::move(race)), name(std::move(name)) {
}

std::string_view Character::get_name() {
    return name;
}

const Race &Character::get_race() const {
    return *race;
}

Race &Character::get_race() {
    return *race;
}
